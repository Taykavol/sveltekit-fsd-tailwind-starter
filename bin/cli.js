#!/usr/bin/env node

import { execSync } from 'child_process';

const runCommand = (command) => {
	try {
		execSync(`${command}`, { stdio: 'inherit' });
	} catch (e) {
		console.error(e);
		return false;
	}
	return true;
};

const repoName = process.argv[2];
const gitCheckoutCommand = `git clone --depth 1 https://gitlab.com/Taykavol/sveltekit-fsd-tailwind-starter ${repoName}`;
const installDepsCommand = `cd ${repoName} && bun install`;
const gitRemoveOrigins = `cd ${repoName} && git remote remove origin`;

// test commit
console.log('Cloning the repository with name ', repoName);
const checkedOut = runCommand(gitCheckoutCommand);
if (!checkedOut) process.exit(-1);

console.log('Installing dependencies...');
const installDeps = runCommand(installDepsCommand);
if (!installDeps) process.exit(-1);

console.log('Removing origins...');
const removedOrigins = runCommand(gitRemoveOrigins);
if (!removedOrigins) process.exit(-1);

console.log('All done!');
