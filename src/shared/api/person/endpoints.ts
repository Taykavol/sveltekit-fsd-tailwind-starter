import { env } from '$env/dynamic/public'
const url = env.PUBLIC_BACKEND_URL

const endpoints = {
    getUsers: '/users',
}

function createUrl(path: string) {
    return `${url}${path}`
}


export function getUsers() {
    const url = createUrl(endpoints.getUsers)
    return fetch(url)
}




