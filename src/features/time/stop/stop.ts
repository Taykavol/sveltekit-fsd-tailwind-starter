export function stop(interval: ReturnType<typeof setInterval>) {
	clearInterval(interval);
}
