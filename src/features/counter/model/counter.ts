import { writable, get } from "svelte/store";

export const counter = writable(0);
const interval = writable<ReturnType<typeof setInterval>>();


export function stopCounter() {
    clearInterval(get(interval));
}

export function startCounter() {
    clearInterval(get(interval));
    const id = setInterval(() => {
        counter.update(x => x + 1);
    }, 1000);

    interval.set(id)
}



