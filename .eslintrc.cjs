module.exports = {
	root: true,
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:svelte/recommended',
		'prettier',
	],
	plugins: ['@typescript-eslint', '@conarti/feature-sliced'],
	rules: {
		'@conarti/feature-sliced/layers-slices': 'error',
		'@conarti/feature-sliced/absolute-relative': 'error',
		'@conarti/feature-sliced/public-api': 'error',
	},

	parser: '@typescript-eslint/parser',
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020,
		extraFileExtensions: ['.svelte']
	},
	env: {
		browser: true,
		es2017: true,
		node: true
	},
	overrides: [
		{
			files: ['*.svelte'],
			parser: 'svelte-eslint-parser',
			parserOptions: {
				parser: '@typescript-eslint/parser'
			}
		}
	]
};
